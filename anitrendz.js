const AnitrendzScrap = require('./Anitrendz-Scrap')

// Get top male character
AnitrendzScrap.getMaleRank()
  .then(data => console.log(data))

// Get top female character
AnitrendzScrap.getFemaleRank()
  .then(data => console.log(data))

// Get top anime
AnitrendzScrap.getAnimeRank()
  .then(data => console.log(data))

// Get top coupleship character
AnitrendzScrap.getCoupleShip()
  .then(data => console.log(data))

// Get top opening theme song
AnitrendzScrap.getOpTheme()
  .then(data => console.log(data))

// Get top ending theme song
AnitrendzScrap.getEdTheme()
  .then(data => console.log(data))