# Anitrendz scrapper
A scrapper for anitrendz to see chart info from [Anitrendz](https://anitrendz.net) using puppeteer and cheerio :tada:

## Installation

```
npm install
```

## Run project

```
npm run start
```

## Basic Usage

Basic usage for get top male and female character, coupleship rank and also top anime chart 

```
const AnitrendzScrap = require('./Anitrendz-Scrap')

// Get top male character
AnitrendzScrap.getMaleRank()
  .then(data => console.log(data))

// Get top anime
AnitrendzScrap.getAnimeRank()
  .then(data => console.log(data))

// Get top female character
AnitrendzScrap.getFemaleRank()
  .then(data => console.log(data))
  
// Get top coupleship character
AnitrendzScrap.getCoupleShip()
  .then(data => console.log(data))
  
 // Get top opening theme song
AnitrendzScrap.getOpTheme()
  .then(data => console.log(data))

// Get top ending theme song
AnitrendzScrap.getEdTheme()
  .then(data => console.log(data))
```

## Resource
* [Puppeteer](https://github.com/puppeteer/puppeteer)
* [Cheerio](https://github.com/cheeriojs/cheerio)

## Author
Satya Wikananda &copy; 2020
