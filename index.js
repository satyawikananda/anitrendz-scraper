const express = require('express')
const app = express()
const cors = require('cors')
const port = 8000
const AnitrendzScrap = require('./Anitrendz-Scrap')

app.use(cors())

app.get('/', (req,res) => {
    res.send({
        test: "Hello world"
    })
})

app.get('/anime-rank', (req,res) => {
    AnitrendzScrap.getAnimeRank()
        .then(data => {
            res.send(data)
        })
        .catch(err => console.log(err))
})

app.get('/couple', (req,res) => {
    AnitrendzScrap.getCoupleShip()
        .then(data => {
            res.send(data)
        })
        .catch(err => console.log(err))
})

app.get('/male', (req,res) => {
    AnitrendzScrap.getMaleRank()
        .then(data => {
            res.send(data)
        })
        .catch(err => console.log(err))
})

app.get('/female', (req,res) => {
    AnitrendzScrap.getFemaleRank()
        .then(data => {
            res.send(data)
        })
        .catch(err => console.log(err))
})

app.get('/opening-song', (req,res) => {
    AnitrendzScrap.getOpTheme()
        .then(data => {
            res.send(data)
        })
        .catch(err => console.log(err))
})

app.get('/ending-song', (req,res) => {
    AnitrendzScrap.getEdTheme()
        .then(data => {
            res.send(data)
        })
        .catch(err => console.log(err))
})

app.use(express.urlencoded({extended: false}))
app.listen(port, () => {
    console.log(`Server listen on port ${port}`)
})