/**
 * @author Satya Wikananda <satyawikananda456@gmail.com>
 * @license MIT
 */

 const puppeteer = require('puppeteer')
 const cheerio = require('cheerio')
 const url = 'https://anitrendz.net/charts/op-theme-songs/'

 const getOpTheme = async () => {
    const browser = await puppeteer.launch({ headless: true })
    const page = await browser.newPage()
    await page.goto(url,{
        waitUntil: 'load',
        timeout: 0
    })
    const content = await page.content()
        .then(html => {
            const $ = cheerio.load(html)
            let image = []
            let song = []
            let singer = []
            let rank = []
            let previousRank = []
            let status = []
            let lastUpd = []
            let season = []

            $('.at-mcc-e-thumbnail > img').each((i,e) => {
                image.push($(e).attr('src'))
            })
            $('.entry-title').each((i,e) => {
                song.push($(e).text().trim())
            })
            $('.entry-detail').each((i,e) => {
                singer.push($(e).text().trim())
            })
            $('.main-rank').each((i,e) => {
                rank.push($(e).text().trim())
            })
            $('.prev.stats-entry > span').each((i,e) => {
                previousRank.push($(e).text().trim())
            })
            $('.at-mcc-e-movement > .arrow-container > img').each((i,e) => {
                status.push($(e).attr('alt'))
            })
            $('.current-page > .at-cth-b-date').each((i,e) => {
                lastUpd.push($(e).text().trim())
            })
            $('.at-cth-top-season').each((i,e) => {
                season.push($(e).text().trim())
            })

            let data = {}
            data.season = season[0]
            data.opTheme = []
            data.credit = `Anitrendz (${lastUpd[0]})`

            let i
            for(i = 0; i < image.length; i++){
                data.opTheme[i] = {
                    image: image[i],
                    song: song[i],
                    singer: singer[i],
                    rank: parseInt(rank[i]),
                    previouslyRank: previousRank[i] == '-' ? '-' :  parseInt(previousRank[i]),
                    status: status[i]
                }
            }
            return data
        })
    return content
 }

 module.exports = getOpTheme