/**
 * @author Satya Wikananda <satyawikananda456@gmail.com>
 * @license MIT
 */

const puppeteer = require('puppeteer')
const cheerio = require('cheerio')

const getAnimeRank = async () => {
    const browser = await puppeteer.launch({headless: true})
    const page = await browser.newPage()
    await page.goto('https://anitrendz.net/charts/top-anime/',{
        waitUntil: 'load',
        timeout: 0
    })
    const content = await page.content()
        .then(html => {
            const $ = cheerio.load(html)
            let image = []
            let anime = []
            let studio = []
            let rank = []
            let previouslyRank = []
            let status = []
            let lastUpd = []
            let season = []

            $('.at-mcc-e-details > .at-mcc-e-thumbnail > img').each((i,e) => {
                image.push($(e).attr('src'))
            })
            $('.entry-title').each((i,e) => {
                anime.push($(e).text().trim())
            })
            $('.entry-detail').each((i,e) => {
                studio.push($(e).text().trim())
            })
            $('.at-mcc-e-rank > .main-rank').each((i,e) => {
                rank.push($(e).text().trim())
            })
            $('.prev.stats-entry > span').each((i,e) => {
                previouslyRank.push($(e).text().trim())
            })
            $('.at-mcc-e-movement > .arrow-container > img').each((i,e) => {
                status.push($(e).attr('alt'))
            })
            $('.current-page > .at-cth-b-date').each((i,e) => {
                lastUpd.push($(e).text().trim())
            })
            $('.at-cth-top-season').each((i,e) => {
                season.push($(e).text().trim())
            })

            let data = {}
            data.season = season[0]
            data.anime = []
            data.credit = `Anitrendz (${lastUpd[0]})`

            let i
            for(i = 0; i < image.length; i++){
                data.anime[i] = {
                    image: image[i],
                    title: anime[i],
                    studio: studio[i],
                    rank: parseInt(rank[i]),
                    previouslyRank: parseInt(previouslyRank[i]),
                    status: status[i]
                }
            }
            return data
        })
    return content
}

module.exports = getAnimeRank