/**
 * @author Satya Wikananda <satyawikananda456@gmail.com>
 * @license MIT
 */

const getMaleRank = require('./getMaleChara.js')
const getAnimeRank = require('./getAnimeRank.js')
const getFemaleRank = require('./getFemaleRank.js')
const getCoupleShip = require('./getCoupleShip.js')
const getOpTheme = require('./getOpTheme.js')
const getEdTheme = require('./getEdTheme.js')

module.exports = {
    getMaleRank,
    getAnimeRank,
    getFemaleRank,
    getCoupleShip,
    getOpTheme,
    getEdTheme
}